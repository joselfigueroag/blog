class UsersController < ApplicationController
  skip_before_action :authorized
  def create
    @user = User.create(user_params)
    if @user.valid?
      token = encode_t({user_id: @user.id})
      render json: {user: @user}, status: :created
    else
      render json: @user.errors, status: :bad_request  
    end
  end

  def login
    @user = User.find_by(email: params[:email])
    if @user && @user.authenticate(params[:password])
      token = encode_t({user_id: @user.id})
      render json: {user: @user, token: token}
    else
      render json: {error: 'Error en credenciales, vuelva a intentar'}, status: :unauthorized
    end
  end

  private

    def user_params
      params.permit(:email, :password)
    end
end
