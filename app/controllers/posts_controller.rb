class PostsController < ApplicationController
  before_action :set_post, only: [:show, :update, :destroy]
  
  def index
    @posts = Post.where(user_id: @user.id).order('created_at DESC')
    puts ('AQUI')
    puts @user.id
  end

  def show

  end

  def create
    @post = Post.new(post_params)
    @post.user_id = @user.id

    if @post.save
      render json: @post, status: :created
    else
      render json: @post.errors, status: :bad_request 
    end
  end

  def update
    if @post.update(post_params)
      render json: @post, status: :ok
    else
      render json: @post.errors, status: :bad_request 
    end
  end

  def destroy
    @post.destroy
  end

  private
    def set_post
      @post = Post.where(user_id: @user.id).find(params[:id])
    end

    def post_params
      params.permit(:title, :content, :image, :category_id)
    end
end