class ApplicationController < ActionController::API
  before_action :authorized
  SECRET = 'SeCuRiTy'
  
  def encode_t(payload)
    JWT.encode(payload, SECRET)
  end

  def auth_header
    request.headers['Authorization']
  end

  def decode_t
    if auth_header
      token = auth_header.split(' ')[1]
      begin
        JWT.decode(token, SECRET, true, algorithm: 'HS256')
      rescue JWT::DecodeError
        nil
      end
    end
  end

  def logged
    if decode_t
      user_id = decode_t[0]['user_id']
      @user = User.find_by(id: user_id)
    end
  end

  def logged_in?
    !!logged
  end

  def authorized
    puts ('AQUI')
    puts request.headers['Authorization']
    puts auth_header.split(' ')[1]
    render json: { message: 'Please log in' }, status: :unauthorized unless logged_in?
  end
end
