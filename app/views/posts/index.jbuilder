json.array! @posts do |post|
  json.id post.id
  json.title post.title
  json.image post.image
  json.categoria post.category.name
  json.created_at post.created_at
end