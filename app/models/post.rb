class Post < ApplicationRecord
  acts_as_paranoid

  belongs_to :user
  belongs_to :category

  validates :title, :content, :category_id, presence: true
  validates :image, presence: true, url: true,  
                format: { with: %r{\.(gif|jpe?g|png|jpg)}i }
end
