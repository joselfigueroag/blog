class User < ApplicationRecord
  has_secure_password
  has_many :posts

  validates :email, presence: true, uniqueness: true, email: true
  validates :password_digest, presence: true
end
