Rails.application.routes.draw do
  
  #users
  post '/auth/sign_up' => 'users#create'
  post '/auth/login' => 'users#login'

  #categories
  resources :categories, only: [:index]

  #posts
  resources :posts
 
end
