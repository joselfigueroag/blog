# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

categories = ['Listas', 'Tutoriales / Guías', 'Consejos', 'Infografías', 'Entrevistas']
categories.each do |category|
   Category.create({ 'name': category })
end

# user = User.create({
#    'email': 'jose@gmail.com',
#    'password': '123456'
#    'email': 'luis@gmail.com',
#    'password': 'luis28'
# }) 

# post = Post.create({
#    'title': 'Prueba',
#    'content': 'Primera prueba de post',
#    'image': 'www.primerapruebaimage.png',
#    'user_id': 1,
#    'category_id': 3
#    'title': 'Segunda Prueba',
#    'content': 'Segunda prueba de post',
#    'image': 'www.segundapruebaimage.png',
#    'user_id': 2,
#    'category_id': 1
# })